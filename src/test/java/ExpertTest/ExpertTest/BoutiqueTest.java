package ExpertTest.ExpertTest;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;


public class BoutiqueTest {

	
	private WebDriver driver;
	private String baseUrl="https://www.expertest.tn/boutique/";
	private BoutiqueObject BoutiqueObject ;
	
	@Before
	public void setUp() throws Exception {
		WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    BoutiqueObject = new BoutiqueObject(driver);
	    driver.manage().window().maximize();
	}

	@Test
	public void test() throws InterruptedException {
		driver.get(baseUrl); 
		assertEquals("Recherche du produit",BoutiqueObject.getRechercheTexte().getText());
		BoutiqueObject.getRechercheInput().sendKeys("Selenium");
		//wait(1);
		//BoutiqueObject.getRechercheButton().click();
		assertEquals("Panier",BoutiqueObject.getTextePanier().getText());
		//assertEquals("Votre panier est vide.",BoutiqueObject.getPanierVide());
		//BoutiqueObject.getFiltrerTexte().click();
		//assertEquals("Recherche de produits...",BoutiqueObject.getRechercheInput());	
		
		
	}
	
	@Test
	public void TestVueGrilleDisplay () throws InterruptedException {
		driver.get(baseUrl);
		
		BoutiqueObject.getVueGrille().click();
            BoutiqueObject.getProductVueGrid().isDisplayed() ;        
	}
	
	@Test
	public void TestVueListeDisplay () throws InterruptedException {
		driver.get(baseUrl);
		
		BoutiqueObject.getVueListe().click();
            BoutiqueObject.getProductVueList().isDisplayed() ;        
	}
	
	@Test 
	public void TestLanguageDisplay ()
	{
		driver.get(baseUrl);
	
		BoutiqueObject.getLangaugeButton().click();
		BoutiqueObject.getLangaugeMenu().isEnabled() ; 
		BoutiqueObject.getLangaugeMenu().isDisplayed();
	
	}
	
	@Test 
	public void TestScollPrix () throws InterruptedException
	
	{
	driver.get(baseUrl);
	Actions action=new Actions(driver);
	//action.dragAndDropBy(BoutiqueObject.getScrollPrix(),0,47);
	action.clickAndHold(driver.findElement(By.xpath("//*[@id=\"woocommerce_price_filter-2\"]/form/div/div[1]/span[1]"))).build().perform();
	action.moveToElement(driver.findElement(By.xpath("//*[@id=\"woocommerce_price_filter-2\"]/form/div/div[1]/span[2]"))).build().perform();
	//action.moveToElement(BoutiqueObject.getScrollPrix(), 0, 500).build().perform();
	//action.moveToElement(driver.findElement(By.xpath("//*[@id=\"woocommerce_price_filter-2\"]/form/div/div[1]/span[2]")))action.
	}

	
	
//	@After
//	public void tearDown() throws Exception {
//driver.quit();
//	}



}
