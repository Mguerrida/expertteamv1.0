package ExpertTest.ExpertTest;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class QuizzTest {
	
	private WebDriver driver ; 
	private String baseUrl ="https://www.expertest.tn/quizz/" ;
	private QuizzPO QuizzPO ;
	
	@Before
	public void setUp() throws Exception {
		
		WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    QuizzPO = new QuizzPO (driver);
	  	driver.manage().window().maximize();
	  	driver.get(baseUrl);
	}
	@Test 
	public void testtitle () {
	String Expected_Title =  QuizzPO.gettitle().toString() ;
   String Actual_Tite  =driver.findElement(By.xpath("//h1[@class='title']")).toString();
 assertEquals(Expected_Title, Actual_Tite);

		
	}
 @Test 
 public void testLeanPage() {
	QuizzPO.getlean().click(); 
	String Actual_Result = driver.getTitle() ;
    // System.out.println(Actual_Result);
    String Expected_Result ="lean Exam - ExperTest Quizz ISTQB" ; 
    assertEquals(Expected_Result, Actual_Result);
    boolean img =  driver.findElement(By.xpath("//img[@src='https://www.expertest.tn/wp-content/uploads/2021/04/lean-3.jpg']")).isDisplayed();
    assertEquals(img,true);
    JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
	driver.findElement(By.xpath("//a[@role='presentation']")).click();
	String text1 =driver.findElement(By.xpath("//*[@id=\"wpProQuiz_145\"]/div[4]/p[2]")).getText();
	assertEquals("The Lean Six Sigma Yellow Belt Examination Mock Exam V1.1", text1);
    driver.findElement(By.xpath("//*[@id=\"Content\"]/div/div/div/div[1]/div/div[3]/div/div[1]/div/ul/li[2]")).click();
    String text2 = driver.findElement(By.xpath("//*[@id=\"wpProQuiz_147\"]/div[4]/p[2]")).getText();
    assertEquals("The Lean Six Sigma Orange Belt Examination Mock Exam V1.1", text2);
    driver.findElement(By.xpath("//li[@aria-labelledby='ui-id-3']")).click();
    String text3 = driver.findElement(By.xpath("//*[@id=\"wpProQuiz_146\"]/div[4]/p[2]")).getText();
    assertEquals("The Lean Six Sigma Green Belt Examination Mock Exam V1.1", text3);
    driver.findElement(By.xpath("//*[@id=\"Content\"]/div/div/div/div[1]/div/div[3]/div/div[2]/div/div/div[2]/a/span[2]")).click();
    String Test1 = driver.getTitle();
    assertEquals("Quizz - ExperTest Quizz ISTQB", Test1);
    
      //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
     // driver.findElement(By.xpath("//*[@id=\"wpProQuiz_145\"]/div[4]/div/input")).click();  
    //  boolean text2 = driver.findElement(By.xpath("//*[@id=\"wpProQuiz_147\"]/div[4]/p[2]")).isDisplayed();
   //   System.out.println(text2);
         	 
 }
	
	@After
	public void tearDown() throws Exception {
		
	    driver.quit();

	}

 
 
}
