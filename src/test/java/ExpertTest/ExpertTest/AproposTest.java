package ExpertTest.ExpertTest;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AproposTest {
	
	private WebDriver driver ; 
	private String baseUrl ="https://www.expertest.tn/a-propos/" ;
	private AproposPO AproposPO ;
	
	@Before
	public void setUp() throws Exception {
		
		WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    
	    AproposPO = new AproposPO(driver);
	  	driver.manage().window().maximize();
	  	driver.get(baseUrl);
	}
	
@Test
 public void testTitle () {
String Actual_Result =driver.getTitle(); 
assertEquals("A propos - ExperTest Quizz ISTQB", Actual_Result);
	
}
@Test 
public void testText1 () {
WebElement Actual_Result2 = AproposPO.gettext1();
WebElement ExpectedResult2  = driver.findElement(By.xpath("//i[@class='icon-users']"));
assertEquals(ExpectedResult2, Actual_Result2);
	
}
@Test 
public void testText2 () {
WebElement Actual_Result3 = AproposPO.gettext2();
WebElement ExpectedResult3  = driver.findElement(By.xpath("//i[@class='icon-lightbulb-line']"));
assertEquals(ExpectedResult3, Actual_Result3);
}
@Test 
public void testText3 () {
WebElement Actual_Result4 = AproposPO.gettext3();
WebElement ExpectedResult4  = driver.findElement(By.xpath("//i[@class='icon-paper-plane']"));
assertEquals(ExpectedResult4, Actual_Result4);
}

@Test 
public void testHisto() {
	WebElement Actual_Result5 =AproposPO.getHisto() ; 
	WebElement Expected_Result5 = driver.findElement(By.xpath("//i[@class='icon-hourglass']"));	
	assertEquals(Expected_Result5, Actual_Result5);
}
@Test
public void testnosval() {
	WebElement Actual_Result6 = AproposPO.getnosval();
	WebElement Expected_Result6 = driver.findElement(By.xpath("//i[@class='icon-cog-line']"));	
	assertEquals(Expected_Result6, Actual_Result6);
}
@Test 
public void testpart() {
	WebElement Actual_Result7 = AproposPO.getpart();
	WebElement Expected_Result7 = driver.findElement(By.xpath("//i[@class='icon-statusnet']"));
	assertEquals(Expected_Result7, Actual_Result7);
}

//@Test 
//public void testStartup () {
//	AproposPO.getStartup().click();
//	Set<String> ids = driver.getWindowHandles();
//	Iterator <String> it = ids.iterator();
//	String ParentID = it.next(); 
//	String ChildID = it.next();
//	driver.switchTo().window(ChildID); 
//    String Actual_Result = driver.getTitle() ;
//    String Expected_Result ="Startup Act Tunisie" ; 
//    assertEquals(Expected_Result, Actual_Result);
//
//}
//
//
//@Test 
//public void testgasq () {
//	AproposPO.getgasq().click();
//	Set<String> ids = driver.getWindowHandles();
//	Iterator <String> it = ids.iterator();
//	String ParentID = it.next(); 
//	String ChildID = it.next();
//	driver.switchTo().window(ChildID); 
//    String Actual_Result = driver.getTitle() ;
//    String Expected_Result ="Accueil - GASQ" ; 
//    assertEquals(Expected_Result, Actual_Result);
//
//}
//@Test
//public void testupload () {
//	AproposPO.getupload().click();
//	Set<String> ids = driver.getWindowHandles();
//	Iterator <String> it = ids.iterator();
//	String ParentID = it.next(); 
//	String ChildID = it.next();
//	driver.switchTo().window(ChildID); 
//    String Actual_Result = driver.getTitle() ;
//    String Expected_Result ="\r\n"
//    		+ "	Computer-Based Test (CBT) development and delivery :: Pearson VUE" ; 
//   
//    assertEquals(Expected_Result, Actual_Result);
//}
@After
public void tearDown() throws Exception {
	
    driver.quit();

}
}
