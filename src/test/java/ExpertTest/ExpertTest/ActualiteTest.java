package ExpertTest.ExpertTest;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ActualiteTest {
	
	private WebDriver driver ; 
	private String baseUrl ="https://www.expertest.tn/actualite/" ;
	private ActualitePO ActualitePO ;
	@Before
	public void setUp() throws Exception {
		
		WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	    
	    ActualitePO = new ActualitePO(driver);
	  	driver.manage().window().maximize();
	  	driver.get(baseUrl);
	}
	@Test
	public void testtile () {
	String Titre = ActualitePO.gettitle1().getText();	
	System.out.println(Titre);
	assertEquals("Actualité", Titre);	
	}
	@Test 
	
	public void testPage1 () {
	Actions action = new Actions (driver);
	WebElement Picture = ActualitePO.getPage1();
	action.moveToElement(Picture).build().perform();
	ActualitePO.getloupe().click();
	assertEquals(driver.findElement(By.className("pp_close")).isEnabled(),true);
//	Set<String> ids = driver.getWindowHandles();
//	Iterator <String> it = ids.iterator();
//	String ParentID = it.next(); 
//	String ChildID = it.next();
//	driver.switchTo().window(ChildID); 	
  //assertEquals(driver.findElement(By.id("fullResImage")).isDisplayed(), true);	
	}
	@Test 
	public void testTextplus() {
	ActualitePO.TextPlus.click();
	ActualitePO.getmontertt_BTN().click();
	assertEquals("Blog - ExperTest Quizz ISTQB", driver.getTitle());		
	}
	
	@Test 
	public void TestPagescrolldown() {
		Actions action = new Actions (driver);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		//WebElement Picture = ActualitePO.getPage2();
//		WebElement Picture2 = ActualitePO.getPage2();
//		action.moveToElement(Picture2).build().perform();
//		ActualitePO.getloupe().click();
//		assertEquals(driver.findElement(By.className("pp_close")).isEnabled(),true);
	}
	
	 
	@After
	public void tearDown() throws Exception {
		
	    driver.quit();

	}
	
}


