package ExpertTest.ExpertTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import ExpertTest.ExpertTest.HeaderObject ;
import io.github.bonigarcia.wdm.WebDriverManager;

public class HeaderTest {


	private WebDriver driver;
	private String baseUrl="https://www.expertest.tn/";
	private HeaderObject HeaderObject ;
	private String TitleFb =""  ; 
	
	@Before
	public void setUp() throws Exception {
		
		
		WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);  
	    HeaderObject = new HeaderObject(driver);   
	    driver.manage().window().maximize();   
	}
   
	@Test
	public void LinkTestContent () {
		
		driver.get(baseUrl); 
		 assertEquals("+216 71 757 591",HeaderObject.getTelFix().getText()); // vérifier le numéro de téléphone Fix		
	     assertEquals("+216 29 674 204",HeaderObject.getTelPortable().getText()); // vérifier le numéro de téléphone Protable	
		 assertEquals("contact@expertunisie.com",HeaderObject.getEmail().getText());// vérifier l@ Email 
		 assertEquals("Acceuil",HeaderObject.getAcceuilLink().getText());
		 assertEquals("A propos",HeaderObject.getAproposLink().getText());
		 assertEquals("E-boutique",HeaderObject.getEboutiqueLink().getText());
		 assertEquals("Quizz",HeaderObject.getQuizzLink().getText());
		 assertEquals("Téléchargement",HeaderObject.getTéléchargementLink().getText());
		 assertEquals("Actualités",HeaderObject.getActualitéLink().getText());
		 assertEquals("Mon Compte",HeaderObject.getMoncompteLink().getText());
		 
	    // HeaderObject.log.info("Examen en ligne ISTQB - Quizz ISTQB Test Logiciel - Test Manager");
		 
//	    // String TitleFb=driver.getCurrentUrl();
//			
//		 // Vérifier si le click sur licone FB renvoie bien sur la page FB  
//		 
//		 
	}
//	
	
	@Test 
	public void check_Display () {
		
		driver.get(baseUrl);
		HeaderObject.getDisplay().isDisplayed();
	}
	
	//@Test 
//	public void LinkTestSurfHome()
//	{
//		driver.get(baseUrl);
//		HeaderObject.getIconeHome().click();
//		Set<String> ids = driver.getWindowHandles() ;
//		 Iterator <String> it = ids.iterator();
//		 String parentID = it.next() ;
//		 String childID = it.next () ;
//		 driver.switchTo().window(childID);
//		 String TitleHome=driver.getCurrentUrl();
//		 assertEquals("https://www.expertest.tn/",TitleHome);  
//	}
	
	@Test
	public void LinkTestSurfFB () {
		driver.get(baseUrl); 
		 HeaderObject.getFacebook().click();
		 Set<String> ids = driver.getWindowHandles() ;
		 Iterator <String> it = ids.iterator();
		 String parentID = it.next() ;
		 String childID = it.next () ;
		 driver.switchTo().window(childID);
		 String TitleFb=driver.getCurrentUrl();
		 assertEquals("https://fr-fr.facebook.com/Expert.Team.Tunisie",TitleFb);  
	}	
	
	@Test
	public void LinkTestSurfYoutube () {
		driver.get(baseUrl); 
		 HeaderObject.getYoutube().click();
		 Set<String> ids = driver.getWindowHandles() ;
		 Iterator <String> it = ids.iterator();
		 String parentID = it.next() ;
		 String childID = it.next () ;
		 driver.switchTo().window(childID);
		 String TitleYoutube=driver.getCurrentUrl();
		 assertEquals("https://www.youtube.com/channel/UCAtz2FDsyadRE9FpfwAJVmA",TitleYoutube);  
	}	
	
	@Test
	public void LinkTestSurfLinkedin () {
		driver.get(baseUrl); 
		 HeaderObject.getLinkedin().click();
		 Set<String> ids = driver.getWindowHandles() ;
		 Iterator <String> it = ids.iterator();
		 String parentID = it.next() ;
		 String childID = it.next () ;
		 driver.switchTo().window(childID);
		 String TitleLinkedin=driver.getCurrentUrl();
		 assertEquals("https://www.linkedin.com/company/expert-team-tunisie",TitleLinkedin);  
	}	
	
	@Test
	public void LinkTestSurfInstagram () {
		driver.get(baseUrl); 
		 HeaderObject.getInstagram().click();
		 Set<String> ids = driver.getWindowHandles() ;
		 Iterator <String> it = ids.iterator();
		 String parentID = it.next() ;
		 String childID = it.next () ;
		 driver.switchTo().window(childID);
		 String TitleInstagram=driver.getCurrentUrl();
		 assertEquals("https://www.instagram.com/expert.team.tn/",TitleInstagram);  
	}	
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}



}
