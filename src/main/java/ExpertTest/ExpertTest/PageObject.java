package ExpertTest.ExpertTest;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.log.Log;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject {
	
	WebDriverWait wait;
	  private WebDriver driver;
	    Logger log;

	    public PageObject(WebDriver driver){
	        this.driver = driver;
	        PageFactory.initElements(driver, this);
	        
	        log = Logger.getLogger(Log.class.getName());

	        log.setLevel(Level.INFO);
	    }
	    
		public WebElement findElement(String xpath)
		
			{
					
			 return  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
					
			}

}
