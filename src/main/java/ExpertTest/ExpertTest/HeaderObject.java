package ExpertTest.ExpertTest;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HeaderObject extends PageObject {

	 @FindBy(xpath="//ul[@class='contact_details']//li[2]//a[1]")    
		private WebElement TelFix;

	 @FindBy(xpath="//a[contains(text(),'+216 29 674 204')]")    
		private WebElement TelPortable;
	 
	 @FindBy(xpath="//header/div[@id='Action_bar']/div[1]/div[1]/ul[1]/li[4]/a[1]")    
		private WebElement Email;
	 
	 @FindBy(xpath="//div[@id='Action_bar']//i[@class='icon-facebook']")    
		private WebElement Facebook;
	 
	 @FindBy(xpath="//div[@id='Action_bar']//i[@class='icon-play']")    
		private WebElement Youtube;
	 
	 @FindBy(xpath="//div[@id='Action_bar']//i[@class='icon-linkedin']")    
		private WebElement Linkedin;
	 
	 @FindBy(xpath="//img[@class='logo-main scale-with-grid']")    
		private WebElement IconeHome;
	 
	 @FindBy(xpath="//span[normalize-space()='Accueil']")    
		private WebElement AcceuilLink;
	 
	 @FindBy(xpath="//span[normalize-space()='A propos']")    
		private WebElement AproposLink;
	 
	 @FindBy(xpath="//span[normalize-space()='E-boutique']")    
		private WebElement EboutiqueLink;
	 
	 @FindBy(xpath="//span[normalize-space()='Quizz']")    
		private WebElement QuizzLink;
	 
	 @FindBy(xpath="//span[normalize-space()='Téléchargement']")    
		private WebElement TéléchargementLink;
	 
	 @FindBy(xpath="//span[normalize-space()='Actualités']")    
		private WebElement ActualitéLink;
	 
	 @FindBy(xpath="//span[normalize-space()='Mon Compte']")    
		private WebElement MonCompteLink;
	 
	//li[@class='tp-revslider-slidesli active-revslide']//div[@class='tp-bgimg defaultimg']
	 
	 @FindBy(xpath="//li[@class='tp-revslider-slidesli active-revslide']//div[@class='tp-bgimg defaultimg']")    
		private WebElement Display;
	 
	 public WebElement getDisplay() {
		return Display;
	}

	public void setDisplay(WebElement display) {
		Display = display;
	}

	public WebElement getMonCompteLink() {
		return MonCompteLink;
	}

	public void setMonCompteLink(WebElement monCompteLink) {
		MonCompteLink = monCompteLink;
	}

	public WebElement getAcceuilLink() {
		return AcceuilLink;
	}

	public void setAcceuilLink(WebElement acceuilLink) {
		AcceuilLink = acceuilLink;
	}

	public WebElement getAproposLink() {
		return AproposLink;
	}

	public void setAproposLink(WebElement aproposLink) {
		AproposLink = aproposLink;
	}

	public WebElement getEboutiqueLink() {
		return EboutiqueLink;
	}

	public void setEboutiqueLink(WebElement eboutiqueLink) {
		EboutiqueLink = eboutiqueLink;
	}

	public WebElement getQuizzLink() {
		return QuizzLink;
	}

	public void setQuizzLink(WebElement quizzLink) {
		QuizzLink = quizzLink;
	}

	public WebElement getTéléchargementLink() {
		return TéléchargementLink;
	}

	public void setTéléchargementLink(WebElement téléchargementLink) {
		TéléchargementLink = téléchargementLink;
	}

	public WebElement getActualitéLink() {
		return ActualitéLink;
	}

	public void setActualitéLink(WebElement actualitéLink) {
		ActualitéLink = actualitéLink;
	}

	public WebElement getMoncompteLink() {
		return MoncompteLink;
	}

	public void setMoncompteLink(WebElement moncompteLink) {
		MoncompteLink = moncompteLink;
	}



	@FindBy(xpath="//span[normalize-space()='Accueil']")    
		private WebElement MoncompteLink;
	 
	 public void setIconeHome(WebElement iconeHome) {
		IconeHome = iconeHome;
	}

	public WebElement getYoutube() {
		return Youtube;
	}

	 public WebElement getIconeHome() {
			return IconeHome;
		}


	public void setYoutube(WebElement youtube) {
		Youtube = youtube;
	}



	public WebElement getLinkedin() {
		return Linkedin;
	}



	public void setLinkedin(WebElement linkedin) {
		Linkedin = linkedin;
	}



	public WebElement getInstagram() {
		return Instagram;
	}



	public void setInstagram(WebElement instagram) {
		Instagram = instagram;
	}



	@FindBy(xpath="//div[@id='Action_bar']//i[@class='icon-instagram']")    
		private WebElement Instagram;
	 
	public HeaderObject(WebDriver driver) {
		// TODO Auto-generated constructor stub
		super(driver);

	}



	public WebElement getTelFix() {
		return TelFix;
	}



	public void setTelFix(WebElement telFix) {
		TelFix = telFix;
	}



	public WebElement getTelPortable() {
		return TelPortable;
	}



	public void setTelPortable(WebElement telPortable) {
		TelPortable = telPortable;
	}



	public WebElement getEmail() {
		return Email;
	}



	public void setEmail(WebElement email) {
		Email = email;
	}



	public WebElement getFacebook() {
		return Facebook;
	}



	public void setFacebook(WebElement facebook) {
		Facebook = facebook;
	}

	


	
}
