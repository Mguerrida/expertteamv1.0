package ExpertTest.ExpertTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MonCompteObject extends PageObject {

	public MonCompteObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//Inspect Element
	
	////label[@for='username']
	@FindBy(name="login")
	 private WebElement button;
	
	
	@FindBy(id="username")
	 private WebElement InputIdentifiant;
	
	
	@FindBy(id="password")
	 private WebElement InputPassword;
	
	
	//Getter & Setter Methods 
	
	public WebElement getInputPassword() {
		return InputPassword;
	}

	
	public WebElement getbutton() {
		return button;
	}
	public WebElement getInputIdentifiant() {
		return  InputIdentifiant;
	}

	
	
	
	public void setInputPassword(WebElement inputPassword) {
		InputPassword = inputPassword;
	}


	public void setInputIdentifiant(WebElement inputIdentifiant) {
		InputIdentifiant = inputIdentifiant;
	}
	
}
