package ExpertTest.ExpertTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class QuizzPO extends PageObject{

	public QuizzPO(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(xpath="//h1[@class='title']")
	private WebElement Title ;
	
	@FindBy(xpath="//i[@class='icon-clock']")
	private WebElement clock ;

	@FindBy(xpath="//body/div[@id='Wrapper']/div[@id='Content']/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/a[1]/div[2]")
	private WebElement Lean ; 
	
	@FindBy(xpath="//*[@id=\"Content\"]/div/div/div/div[1]/div/div[2]/div/div[3]/div/a/div[2]")
	private WebElement Developpement  ; 
	
	@FindBy(xpath="//*[@id=\"Content\"]/div/div/div/div[1]/div/div[2]/div/div[4]/div/a/div[2]")
	private WebElement ISTQB  ; 
	
	@FindBy(xpath="//*[@id=\"Content\"]/div/div/div/div[1]/div/div[2]/div/div[5]/div/a/div[2]")
	private WebElement AgileScrum ; 
	
	@FindBy(xpath="//*[@id=\"Content\"]/div/div/div/div[1]/div/div[2]/div/div[6]/div/a/div[2]")
	private WebElement A4Q ;


public WebElement gettitle () {
	return Title ;

}

public WebElement getclock() {
	return clock ; 
}
public WebElement getlean() {
return Lean ;	
}
public WebElement getdeveloppement() {
	return Developpement ;
}
public WebElement getistqb() {
	return ISTQB ; 
	
}

public WebElement getagilescrum () {
	return AgileScrum ;
	
}
public WebElement geta4q() {
	
	return A4Q ;
}

}
