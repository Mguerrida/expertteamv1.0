package ExpertTest.ExpertTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FooterObject extends PageObject {

	public FooterObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	   //@FindBy(partialLinkText ="Footer")
		//	 private WebElement Footer;
		   
		 // WebElement footer1=driver.findElement(By.xpath(""));
		   
	   @FindBy(xpath="//img[contains(@class,'attachment-medium size-medium')]")    
		private WebElement ExpertIcone;
		 
		@FindBy(xpath="//p[contains(text(),'Notre mission est de vous garantir la réussite dan')]")    
		private WebElement Description;
		
		//Formulaire 
		
		
		@FindBy(xpath="//input[@value=\"Votre nom*\" ]")
		private WebElement Nom;
		
		@FindBy(xpath="//input[@value=\"Votre e-mail*\" ]")
		private WebElement Email;
		
		@FindBy(xpath="//input[@value=\"Objet*\" ]")    
		private WebElement Object;
		
		@FindBy(linkText="//input[@value=\"Votre message (facultatif)*\" ]")    
		private WebElement Message;
//			
//		public WebElement getFooter() {
//			return Footer;
//		}

//		public void setFooter(WebElement footer) {
//			Footer = this.Footer;
//		}

		public WebElement getExpertIcone() {
			return ExpertIcone;
		}

		public void setExpertIcone(WebElement expertIcone) {
			ExpertIcone = expertIcone;
		}

		public WebElement getDescription() {
			return Description;
		}

		public void setDescription(WebElement description) {
			Description = description;
		}

		public WebElement getNom() {
			return Nom;
		}

		public void setNom(WebElement nom) {
			Nom = nom;
		}

		public WebElement getEmail() {
			return Email;
		}

		public void setEmail(WebElement email) {
			Email = email;
		}

		public WebElement getObject() {
			return Object;
		}

		public void setObject(WebElement object) {
			Object = object;
		}

		public WebElement getMessage() {
			return Message;
		}

		public void setMessage(WebElement message) {
			Message = message;
		}

//		public WebElement getEnvoyer() {
//			return Envoyer;
//		}
//
//		public void setEnvoyer(WebElement envoyer) {
//			Envoyer = envoyer;
//		}
}