package ExpertTest.ExpertTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.FindBy;

public class AproposPO extends PageObject {

	public AproposPO(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(linkText="https://www.expertest.tn/a-propos/")
	 private WebElement title;
	
	@FindBy(xpath ="//i[@class='icon-users']")
	private WebElement text1 ;
	
	
	@FindBy(xpath ="//i[@class='icon-lightbulb-line']")
	private WebElement text2 ;
	
	@FindBy(xpath ="//i[@class='icon-paper-plane']")
	private WebElement text3 ;
	
	@FindBy(xpath ="//i[@class='icon-hourglass']")
	private WebElement Histo ;
	
	@FindBy(xpath ="//i[@class='icon-cog-line']")
	private WebElement nosval ;
	
	@FindBy(xpath ="//i[@class='icon-statusnet']")
	private WebElement part ;
	
	@FindBy(xpath="//a[@href=\"https://www.startupact.tn/accueil.html \"]")
	private WebElement Startup ;

	@FindBy(xpath="//img[@src='https://www.expertest.tn/wp-content/uploads/2021/04/gasq-150x69.png']")
	private WebElement gasq ;
	
	@FindBy(xpath="//img[@src='https://www.expertest.tn/wp-content/uploads/2021/04/pv-150x45.png']")
	private WebElement upload ;
	
	public Object getTitle() {
		// TODO Auto-generated method stub
		return title ;
	}
	
	public WebElement gettext1() {
		return text1 ;	
	}
	
	public WebElement gettext2() {
		return text2 ;	
	}
	
	public WebElement gettext3() {
		return text3 ;	
	}
	

	public WebElement getHisto() {
		return Histo ;	
	}
	

	public WebElement getnosval() {
		return nosval ;	
	}
	
	public WebElement getpart() {
		return part ;	
	}

	public WebElement getStartup() {
		return Startup ;
	}

	public WebElement getgasq() {
		return gasq ;
	}
	
	public WebElement getupload() {
		return upload ;
	}
}

