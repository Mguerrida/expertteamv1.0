package ExpertTest.ExpertTest;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class BoutiqueObject extends PageObject {

	public BoutiqueObject(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

  //Elements Inspect
	@FindBy(xpath="//h3[contains(text(),'Recherche du produit')]")
	 private WebElement RechercheTexte;
	@FindBy(xpath="//input[@id='woocommerce-product-search-field-0']")
	 private WebElement RechercheInput;
	@FindBy(xpath="//button[contains(text(),'Recherche')]")
	 private WebElement RechercheButton;
	
	@FindBy(xpath="//h3[normalize-space()='Panier']")
	 private WebElement TextePanier;
	
	@FindBy(xpath="//li[@class='empty']")
	 private WebElement PanierVide ; 
	
	@FindBy(xpath="//ul[@class='select2-selection__rendered']']")
	 private WebElement FiltrerTexte ; 
	
	@FindBy(xpath="//button[normalize-space()='Appliquer']")
	 private WebElement FiltrerButton ; 
			
	@FindBy(xpath="//span[@class='dashicons dashicons-grid-view']")
	 private WebElement VueGrille ;
	
	@FindBy(xpath="//ul[@class='products grid']")
	 private WebElement ProductVueGrid ; 
	
	@FindBy(xpath="//span[@class='dashicons dashicons-exerpt-view']")
	 private WebElement VueListe ;
	
	@FindBy(xpath="//ul[@class='products list']")
	 private WebElement ProductVueList ; 
	
	@FindBy(xpath="//input[@placeholder='Language']")
	 private WebElement LangaugeButton ; 
	
	@FindBy(css="select2-selection--multiple")
	 private WebElement LangaugeMenu ;

	@FindBy(xpath="//h3[normalize-space()='Catégories produits']")
	 private WebElement CatégorieProduit ;

	//@FindBy(xpath="//div[@class='ui-slider-range ui-corner-all ui-widget-header']")
	 //private WebElement ScrollPrix ;
	
	@FindBy(xpath="//div[@class='ui-slider-range ui-corner-all ui-widget-header']")
	 private WebElement ScrollPrix ;
	
	////a[normalize-space()='Voucher de Certification']
	
	
	
	// Getter & Setter Method	
	
	public WebElement getScrollPrix() {
		return ScrollPrix;
	}

	public void setScrollPrix(WebElement scrollPrix) {
		ScrollPrix = scrollPrix;
	}

	public WebElement getLangaugeButton() {
		return LangaugeButton;
	}

	public void setLangaugeButton(WebElement langaugeButton) {
		LangaugeButton = langaugeButton;
	}

	public WebElement getLangaugeMenu() {
		return LangaugeMenu;
	}

	public void setLangaugeMenu(WebElement langaugeMenu) {
		LangaugeMenu = langaugeMenu;
	}

	public WebElement getVueListe() {
		return VueListe;
	}

	public void setVueListe(WebElement vueListe) {
		VueListe = vueListe;
	}

	public WebElement getProductVueGrid() {
		return ProductVueGrid;
	}

	public void setProductVueGrid(WebElement productVueGrid) {
		ProductVueGrid = productVueGrid;
	}

	public WebElement getProductVueList() {
		return ProductVueList;
	}

	public void setProductVueList(WebElement productVueList) {
		ProductVueList = productVueList;
	}
	
	public WebElement getProductGrid() {
		return ProductVueGrid;
	}

	public void setProductGrid(WebElement productVueGrid) {
		ProductVueGrid = productVueGrid;
	}

	
	public WebElement getFiltrerTexte() {
		return FiltrerTexte;
	}

	public WebElement getVueGrille() {
		return VueGrille;
	}

	public void setVueGrille(WebElement vueGrille) {
		VueGrille = vueGrille;
	}

	public void setFiltrerTexte(WebElement filtrerTexte) {
		FiltrerTexte = filtrerTexte;
	}

	
	public WebElement getRechercheButton() {
		return RechercheButton;
	}
	

	public void setRechercheButton(WebElement rechercheButton) {
		RechercheButton = rechercheButton;
	}
	public WebElement getRechercheTexte() {
		return RechercheTexte;
	}

	public void setRechercheTexte(WebElement rechercheTexte) {
		RechercheTexte = rechercheTexte;
	}

	public WebElement getRechercheInput() {
		return RechercheInput;
	}

	public void setRechercheInput(WebElement rechercheInput) {
		RechercheInput = rechercheInput;
	}

	
	public WebElement getTextePanier() {
		return TextePanier;
	}

	public void setTextePanier(WebElement textePanier) {
		TextePanier = textePanier;
	}

	public WebElement getPanierVide() {
		return PanierVide;
	}

	public void setPanierVide(WebElement panierVide) {
		PanierVide = panierVide;
	}


	public WebElement getFiltrerButton() {
		return FiltrerButton;
	}

	public void setFiltrerButton(WebElement filtrerButton) {
		FiltrerButton = filtrerButton;
	}
}
  