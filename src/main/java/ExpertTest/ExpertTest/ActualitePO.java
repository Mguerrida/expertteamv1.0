package ExpertTest.ExpertTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ActualitePO extends PageObject {

	public ActualitePO(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
@FindBy(xpath="//h1[@class='title']")
WebElement title ;

@FindBy(xpath="//a[@class='list-nav']")
WebElement Montrertt_BTN ; 

@FindBy(xpath="//*[@id=\"Content\"]/div/div/div/div[1]/div/div/div/div[2]/div/div/div[1]/div[1]/div[2]/div/a/img[1]") 
WebElement Page1 ;

@FindBy(xpath="//i[@class='icon-search']")
WebElement Loupe ; 

@FindBy(className ="icon-link")
WebElement Link ; 
@FindBy(partialLinkText ="Comment les équipes agiles peuvent")
WebElement TextPlus; 
@FindBy(xpath ="//*[@id=\"Content\"]/div/div/div/div[1]/div/div/div/div[2]/div/div/div[1]/div[10]/div[2]/div/a/img[1]")
WebElement Page2 ;
public WebElement getTextPlus () {
return TextPlus ; 
}
public WebElement gettitle1 () {
return title ; 
}

public WebElement getmontertt_BTN () {
return Montrertt_BTN ; 
}

public WebElement getPage1() {
	return Page1 ;
}

public WebElement getloupe () {
	return Loupe ;
}

public WebElement getlLink() {
	return Link ;
}
public WebElement getPage2() {
	// TODO Auto-generated method stub
	return null;
}
}